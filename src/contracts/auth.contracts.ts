import { Type } from 'class-transformer';
import {
  IsDate,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUrl,
  MaxLength,
  MinDate,
  MinLength,
  ValidateNested,
} from 'class-validator';
import { UserType } from '../app/models/user.model';
import { BaseResponseDTO } from './base.contracts';

const thirteenYearsAgo = new Date();
thirteenYearsAgo.setDate(thirteenYearsAgo.getDate());
thirteenYearsAgo.setMonth(thirteenYearsAgo.getMonth());
thirteenYearsAgo.setFullYear(thirteenYearsAgo.getFullYear());
const minimunYears =
  thirteenYearsAgo.getMonth() + '/' + thirteenYearsAgo.getDate() + '/' + thirteenYearsAgo.getFullYear();

export class AuthSignupRequest {
  @IsNotEmpty()
  @IsString()
  @MinLength(10, { message: 'Phone Number must be at least 10 characters long' })
  phone!: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  fullName!: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(8, { message: 'Password must be at least 8 characters long' })
  @MaxLength(20, { message: 'Password must not exceed 20 characters long' })
  password!: string;

  @IsOptional()
  @IsEmail({}, { message: 'Please provide a valid email address' })
  email?: string;

  @IsOptional()
  @IsDate()
  @MinDate(new Date(minimunYears))
  dob?: string;

  @IsOptional()
  @IsUrl()
  profile_picture?: string;
}

export class AuthSignupResponse extends BaseResponseDTO {
  @IsNotEmpty()
  @IsString()
  token!: string;
}

export class AuthLoginRequest {
  @IsNotEmpty()
  @IsString()
  @MinLength(10, { message: 'Phone Number must be at least 10 characters long' })
  phone!: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(8, { message: 'Password must be at least 8 characters long' })
  @MaxLength(20, { message: 'Password must not exceed 20 characters long' })
  password!: string;
}

export interface TokenPayload {
  phone: string;
  fullName: string;
}

export class AuthLoginResponseDTO {
  @IsNotEmpty()
  @IsString()
  _id!: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(10, { message: 'Phone Number must be at least 10 characters long' })
  phone!: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  fullName!: string;

  @IsNotEmpty()
  @IsEnum(UserType)
  role!: string;
}

export class AuthLoginResponse extends BaseResponseDTO {
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => AuthLoginResponseDTO)
  result!: AuthLoginResponseDTO;
}
