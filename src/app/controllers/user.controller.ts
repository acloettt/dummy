import { Get, HttpCode, JsonController, Param } from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';
import { UserService } from '../services/user.service';

@JsonController('/users')
export class UserController {
  private userService = new UserService();

  @Get('/:phone')
  @HttpCode(200)
  @OpenAPI({ summary: 'Get User Account Details ' })
  async getUserByPhone(@Param('phone') phone: string) {
    const result = this.userService.getUserProfile(phone);

    return { message: 'Successfully Returned Profile', result };
  }
}
