import _ from 'lodash';

import ErrorHandler, { ConflictError, UnprocessableEntityError } from '../../../middlewares/errors';
import { TokenPayload } from '../../../contracts';

import jwt from 'jsonwebtoken';

import { AuthSignupRequest } from '../../../contracts/auth.contracts';
import { UserModel, UserType } from '../../models/user.model';
import logger from '../../../utils/logger';
import config from '../../../utils/config';
import { BadRequestError } from 'routing-controllers';

export class SignupService {
  public async createUserAccount(data: AuthSignupRequest): Promise<string> {
    try {
      if (_.isEmpty(data)) throw new BadRequestError('User Resgistration payload is required');

      const phoneExist = await UserModel.findOne({ phone: data.phone });
      const emailExist = await UserModel.findOne({ email: data.email });

      if (phoneExist) throw new ConflictError('Phone is in use , Login instead');

      if (emailExist) throw new ConflictError('Email is in use , Login instead');

      const newUser = await UserModel.create({
        phone: data.phone,
        fullName: data.fullName,
        password: data.password,
        email: data.email ?? null,
        dob: data.dob ?? null,
        role: UserType.SUBSCRIBER,
        profile_picture: data.profile_picture ?? null,
      });

      if (!newUser) throw new UnprocessableEntityError('There was a problem creating your account');

      const { phone, fullName, _id } = newUser;

      const payload: TokenPayload = {
        phone,
        fullName,
      };

      const token = jwt.sign(payload, config.JWT_SECRET, { expiresIn: config.TOKEN_EXPIRE });

      if (!token) {
        await UserModel.findByIdAndRemove(_id);
        throw new UnprocessableEntityError('Error generating JWT token');
      }

      return token;
    } catch (err: any) {
      logger.error(`[SignupService:createUserAccount]: ${err}`);
      throw new ErrorHandler(err);
    }
  }
}
