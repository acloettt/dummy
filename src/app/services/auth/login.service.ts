import _ from 'lodash';
import { BadRequestError, NotFoundError } from 'routing-controllers';
import { AuthLoginRequest, AuthLoginResponseDTO } from '../../../contracts';
import ErrorHandler, { ConflictError } from '../../../middlewares/errors';
import logger from '../../../utils/logger';
import { UserModel } from '../../models/user.model';

export class LoginService {
  public async loginUser(data: AuthLoginRequest): Promise<AuthLoginResponseDTO> {
    try {
      if (_.isEmpty(data)) throw new BadRequestError('Login Payload is required');

      const userDocument = await UserModel.findOne({ phone: data.phone });

      // check if user exist
      if (!userDocument) throw new NotFoundError('Invalid Login Ceredentials');

      // check if password is valid
      const passwordMatch = userDocument.validatePassword(data.password);

      if (!passwordMatch) throw new ConflictError('Invalid Login Ceredentials');

      // STRIPING OFF PASWORD
      const { phone, fullName, _id, role } = userDocument;

      const loginResponse: AuthLoginResponseDTO = {
        _id: _id.toString(),
        phone,
        fullName,
        role,
      };

      return loginResponse;
    } catch (err: any) {
      logger.error(`[LoginService:createUserAccount]: ${err}`);
      throw new ErrorHandler(err);
    }
  }
}
