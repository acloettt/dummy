import { NotFoundError } from 'routing-controllers';
import ErrorHandler from '../../middlewares/errors';
import logger from '../../utils/logger';
import { UserModel } from '../models/user.model';

export class UserService {
  public async getUserProfile(phone: string) {
    try {
      const userDocument = await UserModel.findOne({ phone }).exec();

      if (!userDocument) {
        throw new NotFoundError('User Does Not Exist');
      }

      return userDocument;
    } catch (err: any) {
      logger.error(`[UserService:getUserAccount]: ${err}`);
      throw new ErrorHandler(err);
    }
  }
}
