import { prop } from '@typegoose/typegoose';

export enum OrganizationType {
  SOLE = 'SOLE',
  PRIVATE = 'PRIVATE',
  PARTNERSHIP = 'PARTNERSHIP',
  CLUB = 'CLUB',
  GOVERNMENT = 'GOVERNMENT',
  NON_PROFIT = 'NON_PROFIT',
}

export enum REGIONS {
  Ashanti = 'Ashanti',
  Bono = 'Bono',
  Bono_East = 'Bono_East',
  Ahafo = 'Ahafo',
  Central = 'Central',
  Eastern = 'Eastern',
  Greater_Accra = 'Greater_Accra',
  Nothern = 'Nothern',
  Savannah = 'Savannah',
  North_East = 'North_East',
  Upper_East = 'Upper_East',
  Upper_West = 'Upper_West',
  Volta = 'Volta',
  Oti = 'Oti',
  Western = 'Western',
  Western_North = 'Western_North',
}

export class OrganizationCollection {
  @prop({ required: true, unique: true })
  public orgId!: string;

  @prop({ required: true, unique: true })
  public name!: string;

  @prop({ required: true, enum: OrganizationType })
  public type!: string;

  @prop({ required: true, unique: true })
  public phone!: string;

  @prop({ unique: true, default: null })
  public email?: string;

  @prop({ required: true, enum: REGIONS })
  public region!: string;

  @prop({ required: true })
  public district!: string;

  @prop({ required: true })
  public town!: string;

  // PHONE NUMBERS OF PEOPLE MADE ADMINS
  @prop({ required: true })
  public admins!: string[];

  // PHONE NUMBERS OF Creator
  @prop({ required: true })
  public pioneer!: string[];

  @prop({ default: null })
  logo?: string;

  @prop({ default: null })
  banner?: string;
}
