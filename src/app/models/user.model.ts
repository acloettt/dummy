import { getModelForClass, prop, modelOptions, Severity, pre, DocumentType } from '@typegoose/typegoose';

import * as crypto from '../../utils/crypto';

// DONT CHANGE ANY OF THE VALUES
// CHANGES TO THIS WILL REQUIRE A DROP IN THE DATABASE
export enum UserType {
  SUBSCRIBER = 'SUBSCRIBER',
  KNIGHT = 'KNIGHT',
  ADMIN = 'ADMIN',
  MODERATOR = 'MODERATOR',
  DEV = 'DEV',
}

/* Typegoose Hook to hash password and save into db record */
@pre<UserCollection>('save', function () {
  if (this.isModified('password') || this.isNew) {
    this.password = crypto.encryptPassword(this.password);
  }
})
/* Declare user class */

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'user' },
})
export class UserCollection {
  @prop({ required: true, unique: true })
  public phone!: string;

  @prop({ required: true, minlength: 5 })
  public fullName!: string;

  @prop({ required: true, default: UserType.SUBSCRIBER, enum: UserType })
  public role!: string;

  @prop({ required: true, minlength: 8, maxlength: 20 })
  public password!: string;

  @prop({ unique: true, default: null })
  public email?: string;

  @prop()
  dob?: string;

  @prop({ default: null })
  profile_picture?: string;

  validatePassword(this: DocumentType<UserCollection>, inputPassword: string) {
    return crypto.validPassword(inputPassword, this.password);
  }
}

export const UserModel = getModelForClass(UserCollection);
