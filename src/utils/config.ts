const dev = {
  mongoUri: 'mongodb+srv://koo:koo@cluster0.hdsob.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
  PORT: 5000,
  HOST: 'http://localhost',
  API_VERSION: '/api/v1',
  TEST_API_PREFIX: '/test',
  JWT_SECRET: '?4cFQ9QPW43v^U2W',
  SECRET_KEY: 'asdfkaosdjfaskdf',
  JWT_ISSUER: 'http://localhost:5000',
  TOKEN_EXPIRE: 4.32e7,
  VERF_EXPIRE: '4h',
  dbName: 'megaplan',
};

const prod = {
  ...dev,
  PORT: process.env.PORT,
};

const environment = {
  test: dev,
  development: dev,
  production: prod,
};

export default environment[process.env.NODE_ENV as 'development' | 'production'];
