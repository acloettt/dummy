export interface GetAllRequestWithParams {
  sort: string;
  limit: string; // The page size. Set 0 for no limit.
  sortField: string; // The field name to query the range for. The field must be:
  /*
      1. Orderable. We must sort by this value. If duplicate values for paginatedField field
        exist, the results will be secondarily ordered by the _id.
      2. Indexed. For large collections, this should be indexed for query performance.
      3. Immutable. If the value changes between paged queries, it could appear twice.
      4. Complete. A value must exist for all documents.
    The default is to use the Mongo built-in '_id' field, which satisfies the above criteria.
    The only reason to NOT use the Mongo _id field is if you chose to implement your own ids.
  */
  sortAscending: boolean; // True to sort using paginatedField ascending (default is false - descending).
  next: string; // The value to start querying the page.
  previous: string; // The value to start querying previous page.
}

/* use for connection classes: e.g. db or pub/sub in tests setup  */
export type DoneCallback = (doneCallback: any) => Promise<any>;
export type TCallbackFunction = () => Promise<any>;
export interface ConnectDbInterface {
  connect: () => void;
  disconnect: DoneCallback;
}
