import { validationMetadatasToSchemas } from 'class-validator-jsonschema';
import { defaultMetadataStorage } from 'class-transformer/storage';
import { routingControllersToSpec } from 'routing-controllers-openapi';
import { getMetadataArgsStorage } from 'routing-controllers';
import { routingControllersOptions } from '../server';
import config from './config';
// Parse class-validator classes into JSON Schema:
const schemas = validationMetadatasToSchemas({
  classTransformerMetadataStorage: defaultMetadataStorage,
  refPointerPrefix: '#/components/schemas/',
});

// Parse routing-controllers classes into OpenAPI spec:
const storage = getMetadataArgsStorage();
const swaggerConfig = routingControllersToSpec(storage, routingControllersOptions, {
  components: {
    schemas,
    // securitySchemes: {
    //   basicAuth: {
    //     scheme: 'basic',
    //     type: 'http',
    //   },
    // },
  },
  servers: [{ url: `${config.HOST}:${config.PORT}${config.API_VERSION}` }],
  info: {
    description: 'Created with ❤❤❤ by ` 😎 Evans Obeng  @iamevansobeng`',
    title: '👋 Megaplan Api',
    version: '0.1.0 Beta',
  },
});

export default swaggerConfig;
