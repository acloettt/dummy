import 'reflect-metadata';
import { createExpressServer, RoutingControllersOptions } from 'routing-controllers';
import { routingControllersOptions } from '../src/server';
import { TestingController } from './controllers/errors.mock';

import { AuthController, UserController } from '../src/app/controllers';
import config from '../src/utils/config';
// import connectMongo from './db';
// connectMongo();

const options: RoutingControllersOptions = {
  ...routingControllersOptions,
  controllers: [TestingController, UserController, AuthController],
  routePrefix: `${config.TEST_API_PREFIX}`,
};

const server = createExpressServer(options);
export default server;
