import mongoose from 'mongoose';
import config from '../src/utils/config';
import logger from '../src/utils/logger';

mongoose.connect(config.mongoUri, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  dbName: 'test',
  useFindAndModify: false,
  useCreateIndex: true,
});

const database = mongoose.connection;

database.on('error', console.error.bind(console, 'connection error'));
database.once('open', () => {
  logger.debug('connected to database');
});

export default database;
