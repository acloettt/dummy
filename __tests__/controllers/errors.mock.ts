import 'reflect-metadata';
import { Get, JsonController } from 'routing-controllers';
import ErrorHandler, { ConflictError, UnprocessableEntityError } from '../../src/middlewares/errors';

@JsonController('/test-controller')
export class TestingController {
  @Get('/unprocessable-entity')
  getUnprocessableEntity() {
    try {
      throw new UnprocessableEntityError('We cant process your request');
    } catch (error: any) {
      throw new ErrorHandler(error);
    }
  }

  @Get('/conflict-error')
  conflictError() {
    try {
      throw new ConflictError();
    } catch (error: any) {
      throw new ErrorHandler(error);
    }
  }
}
