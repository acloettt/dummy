import config from '../src/utils/config';

export const loginRoute = `${config.TEST_API_PREFIX}/auth/login`;
export const signupRoute = `${config.TEST_API_PREFIX}/auth/signup`;

export const userRoute = `${config.TEST_API_PREFIX}/users`;
